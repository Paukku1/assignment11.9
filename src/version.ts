import express, {Request, Response} from 'express'

const router = express.Router()
router.use(express.json())

router.get('/', (req: Request, res: Response) => {
    res.status(200).send('Versio: 2')
})

export default router