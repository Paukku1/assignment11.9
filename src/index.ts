import express, {Request, Response} from 'express'
import  {checkIsValidtoPost, timeParser} from './middleware'
import version from './version'

const server = express()
server.use(express.json())
server.use('/version', version)

export interface Events {
    id: number,
    title: string,
    description: string,
    date: Date,
    time?: string
}

export let events: Events[] = [
    {id:1, title: 'Kesäpäivä', description:'Vietä kesää musiikkia kuunnellen', date:new Date('2023-7-12')},
    
    {id:2, title: 'Kesäyö', description:'Vietä yö järvellä', date:new Date('2023-7-13')},
    
    {id:3, title: 'Kesäfestivaali', description:'Lähde festarikiertueelle', date:new Date('2023-6-12')}
] 

server.get('/', (req: Request, res: Response) => {
    const result = timeParser(events)
    
    res.set(200).send(result)
})

server.get('/:monthNumber', (req: Request, res: Response) => {
    const monthNumber = Number(req.params.monthNumber)
    const monthsEvents = events.filter((event) => {
        return event.date.getMonth() === monthNumber -1
    })

    const result = timeParser(monthsEvents)
    res.json(result)
})

server.post('/', checkIsValidtoPost, (req: Request, res: Response) => {
    
    const {title, description, date, time} = res.locals
    const id = events.length > 0 ? events[events.length-1].id + 1 : 1

    if(time === undefined) {
        const newEvent: Events = {id, title, description, date}
        events.push(newEvent)
    } else {
        const newEvent: Events = {id, title, description, date, time}
        events.push(newEvent)
    }

    const result = timeParser(events)

    res.status(200).send(result)
})

server.put('/:eventId', (req: Request, res: Response) => {
    const id = parseInt(req.params.eventId)
    const {title, description, date, time} = req.body

    const findEvent = events.find(event => { 
        return event.id === id
    })
    
    if(findEvent === undefined) return res.status(404).send({error: 'No match for that ID. Check ID'})
    if(title === undefined && description === undefined && date === undefined && time === undefined) return res.status(401).send({error: 'Nothing can change'})

    if(title) {
        findEvent.title = title
    }
    if(description) {
        findEvent.description = description
    }
    if(date) {
        findEvent.date = date
    }
    if(time) {
        findEvent.time = time
    }

    const result = timeParser(events)
    res.status(200).send(result)
})

server.delete('/:eventId', (req: Request, res: Response) => {
    const id = parseInt(req.params.eventId)

    const findEvent = events.find(event => event.id === id )
    
    if(findEvent === undefined) return res.status(404).send({error: 'No match for that ID. Check ID'})
    events = events.filter(event => event.id !== id)

    const result = timeParser(events)
    res.status(200).send(result)
    
})

server.listen(3000, () => console.log('Listening to port', 3000))

